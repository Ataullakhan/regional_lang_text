from django.apps import AppConfig


class RegionalLangTextAppConfig(AppConfig):
    name = 'regional_lang_text_app'
