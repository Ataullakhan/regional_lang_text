from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from regional_lang_text import settings
from pdf2image import convert_from_path
import json
import os
import cv2
import requests
import png
import re
path = settings.MEDIA_ROOT


def extract_pixlab(request):
    """
    # given pdf file of regional text
    :return: extracted text
    """
    if os.path.exists(path + "/converted_img.png"):
        os.remove(path + "/converted_img.png")
    try:
        txt = ''
        mr_date = ''
        village = ''
        city = ''
        taluka = ''
        filename = ''
        if request.method == 'POST' and request.FILES['pdf_file']:
            pdf_file = request.FILES['pdf_file']
            fs = FileSystemStorage()
            filename = fs.save(pdf_file.name, pdf_file)

            file_name = path + '/' + filename

            pages3 = convert_from_path(file_name, 500)[0]

            # for page in pages3:
            pages3.save(path + '/converted_img.png', 'png')
            file3 = path + '/converted_img.png'

            image = cv2.imread(file3)
            image = cv2.fastNlMeansDenoisingColored(image, None, 10, 10, 7, 21)
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            # dst = preprocess(gray, THRESHOLD)

            png.from_array(gray, 'L').save(path + "/image.png")


            # store image in pixlab.xyz storage
            req = requests.post('https://api.pixlab.io/store',
                                files={'file': open(path + "/image.png", 'rb')},
                                data={
                                    'comment': 'Super Secret Stuff',
                                    'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
                                }
                                )
            reply = req.json()
            # Extracting text from genrated image link with ocr.
            req = requests.get('https://api.pixlab.io/ocr', params={
                'img': reply['link'],
                'key': '3f14cce2d5dba6c3fe95e9c13ddc0fc8',
                'orientation': True,
                'nl': True})
            reply = req.json()
            txt = reply['output']

            mr_txt = re.finditer(r'(\bअहवाल दिनांक*)(.*)', txt)
            for client_txt in mr_txt:
                mr_date = client_txt.group()

            mr_txt = re.finditer(r'(\bगाव*)(.*)', txt)
            for client_txt in mr_txt:
                village = client_txt.group()

            mr_txt = re.finditer(r'(\bजिल्हा*)(.*)', txt)
            for client_txt in mr_txt:
                city = client_txt.group()

            mr_txt = re.finditer(r'(\bतोलुका*)(.*)', txt)
            for client_txt in mr_txt:
                taluka = client_txt.group()

        return render(request, 'home.html', {'filename': filename,'text': txt,
                                             "mr_date":mr_date,
                                             "village":village,
                                             "city": city,
                                             "taluka": taluka})
    except Exception as e:
            print(e)
            return render(request, 'home.html', {})

